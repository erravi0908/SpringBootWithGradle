package com.example.springwithgradle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.oas.annotations.EnableOpenApi;


@SpringBootApplication
@EnableOpenApi
@ComponentScan(basePackages = { "io.swagger", "io.swagger.api" , "io.swagger.configuration"})
public class SpringwithgradleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringwithgradleApplication.class, args);
		
		System.out.println("WELCOME TO SPRINGBOOT WITH GRADLE APPLICATION");
		
	}

}
